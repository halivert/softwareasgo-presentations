import Reveal from "reveal.js";
import Zoom from "reveal.js/plugin/zoom/zoom.esm";

import "reveal.js/dist/reveal.css";
import "reveal.js/dist/theme/sky.css";

Reveal.initialize({
	hash: true,
	history: true,
	plugins: [Zoom],
});
